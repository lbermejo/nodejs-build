# README #

Used to customize a nodejs container with postgres database (separate container or Bitbucket's own instance)

### What is this repository for? ###

* Dockerfile for generating docker environment for nodejs apps to be used for CI
* Bitbucket-pipelines config file

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact