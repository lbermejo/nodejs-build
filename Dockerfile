FROM node:4.2.6

# Create app directory in machine
WORKDIR /usr/src/app

# local proj src. WE need to grab the package.json
ENV PROJ_SRC ../websafety-2-server

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY ${PROJ_SRC}/package*.json ./

#Environment dependencies
RUN npm install
RUN npm install -g gulp
# If you are building your code for production
# RUN npm install --only=production

#TODOs
# copy dynamodb_local
# install elastic search and up
# start dynamodb
# setup databases
# run script migration imports

